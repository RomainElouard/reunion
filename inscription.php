<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Inscription réunion</title>
    <meta name="author" content="Kilik33">
    <meta name="description" content="Inscription réunion">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        h1{
           text-align: center;
        }
    </style>
</head>

<body>
    <!-- debut page -->
    <h1>Inscription à la réunion <?php echo $_GET['lieu'] ?></h1>
    <form action="index.php?lieu=<?php echo $_GET['lieu'] ?>" method="post" class="col-5 offset-3">
  <div class="form-group">
    <label for="nom">Nom</label>
    <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom">
  </div>
  <div class="form-group">
    <label for="prenom">Prénom</label>
    <input type="text" class="form-control" id="prenom" name="prenom"placeholder="Prénom">
  </div>
  <div class="form-group">
    <label for="mail">Adresse mail</label>
    <input type="text" class="form-control" id="mail" name="mail" placeholder="Adresse mail">
  </div>
  <div class="form-group">
    <label for="telephone">Téléphone</label>
    <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Numéro de téléphone">
  </div>
  <button type="submit" class="btn btn-primary">S'enregistrer à la réunion</button>
  <a class="btn btn-info" href='index.php' >Liste des réunions</a>
</form>


    <!-- fin page -->
</body>
</html>