<!DOCTYPE html>
<html lang="fr">

<head>
   <meta charset="UTF-8">
   <title>Inscription réunion</title>
   <meta name="author" content="Kilik33">
   <meta name="description" content="Inscription réunion">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <!-- Animate.css -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
   <style>
      h1 {
         text-align: center;
         margin-bottom: 30px;
      }

      .succes {
         margin-top: 20px !important;
      }
   </style>
</head>

<body>
   <!-- debut page -->

   <?php
/* Récupère la date du jour */
$dateRecu = date('U');
$dateJour= $dateRecu - 86400;
/* Date du jour - 10 jours */
$DatePast = $dateRecu - 864000;
/* Connexion à la base MongoDB */
   require 'vendor/autoload.php'; //charge tout ce que j'ai installé via composer
   $connexion = (new MongoDB\Client("mongodb://127.0.0.1:27017"))->dbtest->reunion;
/* Enregistrement de la réunion lors de la transition de la page réunion à index */
if (!empty($_POST['lieu']) && !empty($_POST['date']) && !empty($_POST['debut']) && !empty($_POST['theme']) && !empty($_POST['duree'])) {
   $insertOneResult = $connexion->insertOne([
      'lieu' => $_POST['lieu'],
      'date' => $_POST['date'],
      'debut' => $_POST['debut'],
      'theme' => $_POST['theme'],
      'duree' => $_POST['duree']
      ]);
      //Affichage du message pour confirmer que la réunion a bien été enregistrée.
      echo "<div class=\"alert alert-success col-6 offset-3   animated bounceInDown\" role=\"alert\">La réunion qui a lieu le " . $_POST['date'] . " à  " . $_POST['lieu'] . ", a bien été enregistrée ! <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
      <span aria-hidden=\"true\">&times;</span>
    </button></div>";
   }else if(isset($_POST['lieu']) && $_POST['lieu'] == "" || isset($_POST['date']) && $_POST['date'] == "" || isset($_POST['debut']) && $_POST['debut'] == "" || isset($_POST['theme']) && $_POST['theme'] == "" || isset($_POST['duree']) && $_POST['duree'] == ""){
      //Problème lors de la requête et ajout de la réunion impossible en base donc message d'erreur.
      echo "<div class=\"alert alert-danger col-6 offset-3\" role=\"alert\">Tous les champs pour l'ajout d'une réunion n'ont pas été rempli et n'a pu être enregistré. Veuillez recommencer!</div>";
   }
/* Rajout d'une personne */
if (!empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['mail']) && !empty($_POST['telephone']) && !empty($_GET['lieu'])) {
   $connexion->updateOne(
      ['lieu' => $_GET['lieu']],
      ['$push' => ['inscrit' => ['nom' => $_POST['nom'], 'prenom' => $_POST['prenom'] , 'mail' => $_POST['mail'] , 'telephone' => $_POST['telephone']]]]
   );
   //Affichage du message pour confirmer que la personne a bien été enregistrée.
   echo "<div class=\"alert alert-success col-6 offset-3   animated bounceInDown\" role=\"alert\">" . $_POST['prenom'] . " " . $_POST['nom'] . ", a bien été enregistrée pour la réunion qui à lieu à " . $_GET['lieu'] . "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
      <span aria-hidden=\"true\">&times;</span>
    </button></div>";
}else if(isset($_POST['nom']) && $_POST['nom'] == "" || isset($_POST['prenom']) && $_POST['prenom'] == "" || isset($_POST['mail']) && $_POST['mail'] == ""){
   //Problème lors de la requête et inscription impossible en base donc message d'erreur.
   echo "<div class=\"alert alert-danger col-6 offset-3\" role=\"alert\">Tous les champs pour l'ajout d'une personne n'ont pas été rempli et n'a pu être enregistré. Veuillez recommencer!</div>";
}
?>
   <h1>Liste des réunions</h1>

   <table class="table table-striped col-8 offset-2">
      <thead>
         <tr>
            <th scope="col">Lieu</th>
            <th scope="col">Date</th>
            <th scope="col">Heure du début</th>
            <th scope="col">Thème</th>
            <th scope="col">Durée</th>
            <th scope="col">Inscrits</th>
            <th scope="col">Inscription</th>
         </tr>
      </thead>
      <tbody>
         <?php
                  /* Requête pour mettre en ordre par date */
                  $filter = [];
                  $tri = ['sort' =>['date' => 1]];
                  $tableau = $connexion->find($filter, $tri);
                  /* Boucle qui va permettre de mettre la liste */
                  foreach ($tableau as $key => $value) {
                     /* Fonction qui permet de vérifier la date */
                     $dateListe = strtotime($value['date']);
                     if ($dateListe <= $dateJour) {
                        if ($dateListe <= $DatePast) {
                           $connexion->deleteOne(['lieu' => $value['lieu']]);
                        }
                     }else{
                        $disabled ='';
                        $complet ='inscription';
                        $inscrit = 0;
                     /* Fonction pour permettre le blocage du boutton selon le nombre d'inscrits */
                        if (isset($value['inscrit'])) {
                           $tableauNom = $connexion->find(['lieu'=>$value['lieu']]);
                           foreach ($tableauNom as $key => $valeur) {
                              foreach ($valeur['inscrit'] as $key => $valeur) {
                                 $inscrit = $key+1;
                                 if ($key>=4) {/* Si il y'a plus de 5 inscrits (0 à 4) */
                                    $complet = 'complet';
                                    $disabled = "disabled";
                                 }else {
                                    $disabled ='';
                                 }
                              }
                           }
                        }
                        echo "<tr><td>" . $value['lieu'] . "</td><td>" .  $value['date'] . "</td><td>" .  $value['debut'] . "</td><td>" .  $value['theme'] . "</td><td>" .  $value['duree'] . "</td><td><button type=\"button\" class=\"btn btn-light\" data-toggle=\"modal\" data-lieu=\"" . $value['lieu'] . "\"data-target=\"#bd-example-modal-lg\">" . $inscrit . "</button></td><td><a href=\"inscription.php?lieu=".$value['lieu']."\"><button class=\"btn btn-outline-primary\"  role=\"button\"". $disabled .">". $complet ."</button></a></td></tr>";
                     }
                  }
                  ?>
      </tbody>
   </table>
   <div class='essai'></div>
   <a class="btn btn-light col-6 offset-3" href='reunion.php'>Ajouter une réunion</a>


   <!-- Modal -->

   <div class="modal fade" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Liste des inscrits</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         ...
      </div>
    </div>
  </div>
</div>

<!-- Liste des noms -->

<div class="liste des noms"></div>

   <!-- fin page -->
   <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   <!-- ////// $.get NE FONCTIONNE PAS AVEC LA LIBRAIRIE PAR DEFAUT BOOTSTRAP //// -->
   <!-- PRENDRE CELLE DE GOOGLE -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
   crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
   crossorigin="anonymous"></script>
   
   
   <!-- JQuery -->
   
   <script>
      
         $(document).ready(function(){

            $('#bd-example-modal-lg').on('show.bs.modal', function (e) {
               var button = $(e.relatedTarget);
               var lieu = button.data('lieu');
               $(this).find('.modal-title').html('Liste des inscrits pour la réunion qui à lieu à : ' + lieu);
               lieu = encodeURIComponent(lieu);
               $(this).find('.modal-body').load('http://localhost/tdmongo/liste.php?lieu='+lieu);
            /* $('.essai').load('http://localhost/tdmongo/liste.php'); */
            console.log(lieu);
            console.log(e);
         })

         })//Fin document.ready
   </script>
</body>

</html>