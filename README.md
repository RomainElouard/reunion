Application développé avec PHP et une base de donnée MongoDB qui permet d'organiser des réunions avec des inscriptions

L'application se développe en 2 parties:
    -1 Partie Réunion :
        Un formulaire de saisie permet de créer une réunion :
            • Lieu
            • Date
            • Heure de début
            • Thème
            • Durée
            
        Une page affiche la liste et permet de visulaiser toutes les réunions.
        Cette liste affiche les réunions par ordre de date. C'est à dire la prochaine réunion est affichée en premier.
        
    -2 Partie utilisateur :
        Une page affiche un formulaire pour s'inscrire à la réunion sélectionné. et comprendra :
            • Nom
            • Prenom
            • Mail
            • Téléphone

    -Autres fonctionnalités:
        Une réunion qui a plus de 5 inscrits ne proposera plus d’inscription, le bouton « s’inscrire » sera remplacé par une mention « Complet ».
        Une réunion passée n’est plus proposée à l’inscription et est supprimé au bout de 10 jours.
        Il est possible de visualiser une liste de chaque inscrit à une réunion.
        Un message validera l'enregistrement d'une réunion ou d'un utilisateur.