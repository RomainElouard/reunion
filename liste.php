<?php
/* Connexion à la base MongoDB */
require 'vendor/autoload.php'; //charge tout ce que j'ai installé via composer
$connexion = (new MongoDB\Client("mongodb://127.0.0.1:27017"))->dbtest->reunion;
/* Début du tableau */
$tableau = "<table class=\"table table-striped col-8 offset-2\">
      <thead>
         <tr>
            <th scope=\"col\">Prénom</th>
            <th scope=\"col\">Nom</th>
            <th scope=\"col\">Mail</th>
            <th scope=\"col\">Numéro</th>
         </tr>
      </thead>
      <tbody>";
/* Boucle qui va permetre de réucpérer la liste */
$tableauNom = $connexion->find(['lieu'=>$_GET['lieu']]);
foreach ($tableauNom as $key => $valeur) {
   if (isset($valeur['inscrit'])) {
      foreach ($valeur['inscrit'] as $key => $value) {
         $tableau.= "<tr><td>" . $value['prenom'] . "</td><td>" .  $value['nom'] . "</td><td>" .  $value['mail'] . "</td><td>" .  $value['telephone'] . "</td></tr>";
      }
   }
}
$tableau .= "</tbody></table>";
echo $tableau;
?>