<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Inscription réunion</title>
    <meta name="author" content="Kilik33">
    <meta name="description" content="Inscription réunion">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        h1{
           text-align: center;
        }
    </style>
</head>

<body>
    <!-- debut page -->
    <h1>Rajouter une réunion</h1>
    <form action="index.php" method="post" class="col-5 offset-3">
  <div class="form-group">
    <label for="lieu">Lieu</label>
    <input type="text" class="form-control" id="lieu" name="lieu" placeholder="Ajouter le lieu de la réunion">
  </div>
  <div class="form-group">
    <label for="date">Date</label>
    <input type="date" class="form-control" id="date" name="date"placeholder="Date de la réunion">
  </div>
  <div class="form-group">
    <label for="debut">Heure du début</label>
    <input type="text" class="form-control" id="debut" name="debut" placeholder="Heure du début de la réunion">
  </div>
  <div class="form-group">
    <label for="theme">Thème</label>
    <input type="text" class="form-control" id="theme" name="theme" placeholder="Thème de la réunion">
  </div>
  <div class="form-group">
    <label for="duree">Durée de la réunion</label>
    <input type="text" class="form-control" id="duree" name="duree" placeholder="Durée de la réunion">
  </div>
  <button type="submit" class="btn btn-primary">Enregistrer la réunion</button>
  <a class="btn btn-info" href='index.php' >Liste des réunions</a>
</form>


    <!-- fin page -->
</body>
</html>